package models

import (
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type BesplatkaRule struct {
	gorm.Model

	CityId             string `gorm:"not null;primary_key"`
	CategoryId         string `gorm:"not null;primary_key"`
	CategoryTreePath   string `gorm:"not null"`
	KidstaffCategoryId string `gorm:"not null"`

	RuleJSON     []byte            `sql:"type:json" gorm:"not null"`
	RuleMap      map[string]string `json:"data" sql:"-" gorm:"-"`
	RequiredTags pq.StringArray    `gorm:"type:varchar(64)[]"`
}
