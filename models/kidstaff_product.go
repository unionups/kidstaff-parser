package models

import (
	"time"
)

type KidstaffProduct struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	URL       string     `gorm:"not null;`
	Category  string     `gorm:"not null;`
}

//gorm:"default:CURRENT_TIMESTAMP"
