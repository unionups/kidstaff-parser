package models

import (
	"github.com/jinzhu/gorm"
)

type KidstaffCategory struct {
	gorm.Model
	Depth            int               `gorm:"not null;"`
	Name             string            `gorm:"not null;"`
	Url              string            `gorm:"not null;unique;"`
	Last             bool              `sql:"default:false"`
	ParentCategory   *KidstaffCategory `gorm:"foreignkey:ParentCategoryId;"`
	ParentCategoryId uint
}
