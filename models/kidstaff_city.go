package models

import (
	"github.com/jinzhu/gorm"
)

type KidstaffCity struct {
	gorm.Model
	CityId         string         `gorm:"unique;not null;primary_key"`
	CityURL        string         `gorm:"not null"`
	RegionId       string         `gorm:"not null"`
	Name           string         `gorm:"not null"`
	KidstaffRegion KidstaffRegion `gorm:"foreignkey:RegionId"association_foreignkey:RegionId`
}
