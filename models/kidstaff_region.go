package models

import (
	"github.com/jinzhu/gorm"
)

type KidstaffRegion struct {
	gorm.Model
	RegionId string `gorm:"not null;primary_key"`
	Name     string `gorm:"not null"`
}
