package models

import (
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type BesplatkaCategory struct {
	gorm.Model
	IdItem      string         `json:"id_item" gorm:"unique;not null;primary_key"`
	Depth       string         `json:"depth" gorm:"not null;"`
	ChildrenIds pq.StringArray `gorm:"type:varchar(100)[]"`
	Name        string         `json:"name" gorm:"not null;"`
	Last        bool           `json:"last"`
	// Subcategories []*BesplatkaCategory `gorm:"many2many:besplatka_subcategories;association_jointable_foreignkey:subcategory_id"`
	// ParentCategory   *BesplatkaCategory `gorm:"foreignkey:ParentCategoryID;association_foreignkey:IdItem"` // use UserRefer as foreign key
	ParentCategoryId string
}
