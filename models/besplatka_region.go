package models

import (
	"github.com/jinzhu/gorm"
	// "github.com/satori/go.uuid"
	// "time"
)

type BesplatkaRegion struct {
	gorm.Model

	// Id       uuid.UUID
	RegionId        string          `json:"region_id" binding:"required" gorm:"unique;not null;primary_key"`
	Name            string          `json:"name" binding:"required" gorm:"unique;not null"`
	BesplatkaCities []BesplatkaCity `gorm:"foreignkey:RegionId;association_foreignkey:RegionId""`
	// CreatedAt time.Time `json:"created_at"`
	// UpdatedAt time.Time `json:"updated_at"`
}

// func (task *Task) BeforeCreate(scope *gorm.Scope) error {
// 	scope.SetColumn("CreatedAt", time.Now())
// 	scope.SetColumn("ID", uuid.NewV4().String())
// 	return nil
// }

// func (task *Task) BeforeUpdate(scope *gorm.Scope) error {
// 	scope.SetColumn("UpdatedAt", time.Now())
// 	return nil
// }
