package main

import (
	"context"
	"fmt"
	"github.com/alioygur/gores"
	"github.com/fatih/color"
	"github.com/jasonlvhit/gocron"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/parsers/kidstaff-parser/controllers"
	"github.com/parsers/kidstaff-parser/database"
	"log"
	"net/http"
	// "reflect"
	"time"
	// "os"
)

func startHttpServer(port, xml_file_name, delete_json_file_name string) *http.Server {
	srv := &http.Server{Addr: port}

	// Download XML file
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		d := time.Now()
		year, month, day := d.Date()
		file_path := fmt.Sprintf("./XML_FILES/detskiy-mir-besplatka-kidstaff-%v-%v-%v.xml", year, month, day)
		err := gores.Download(w, r, file_path, xml_file_name)
		if err != nil {
			log.Println(err.Error())
		} else {
			color.Cyan("Parser Serve file: %s\nWITH FILENAME: %s\n", file_path, xml_file_name)
		}
	})

	// Download XML file
	http.HandleFunc("/delete", func(w http.ResponseWriter, r *http.Request) {

		d := time.Now()
		year, month, day := d.Date()
		file_path := fmt.Sprintf("./XML_FILES/DELETE_JSON_FILES/delete-%v-%v-%v.json", year, month, day)
		err := gores.Download(w, r, file_path, delete_json_file_name)

		if err != nil {
			log.Println(err.Error())
		} else {
			color.Cyan("Parser Serve file: %s\nWITH FILENAME: %s\n", file_path, delete_json_file_name)
		}
	})

	// go func() {
	color.Green("XML file Server gracefully Started ^)^ ")
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("ListenAndServe(): %s", err)
	}
	// }()

	return srv
}

func main() {
	//////Initialize the databases
	database.InitDBs()
	database.GetDB().LogMode(false)
	defer database.DBsClose()

	//// Run database migrations
	database.Migrate()
	//// Initialize controllers
	controllers.Initialize()

	sheduler_conf := controllers.ApplicationConfig.Sheduler
	if sheduler_conf.IsEnable {
		sheduler_values := map[string]interface{}{
			"Day": (*gocron.Job).Day, "Days": (*gocron.Job).Days,
			"Sunday": (*gocron.Job).Sunday, "Monday": (*gocron.Job).Monday, "Tuesday": (*gocron.Job).Tuesday,
			"Wednesday": (*gocron.Job).Wednesday, "Thursday": (*gocron.Job).Thursday,
			"Friday": (*gocron.Job).Thursday, "Saturday": (*gocron.Job).Thursday,
		}

		sheduler := gocron.NewScheduler()

		f := sheduler_values[sheduler_conf.BySpecific].(func(*gocron.Job) *gocron.Job)
		f(sheduler.Every(sheduler_conf.RunEvery)).At(sheduler_conf.AtTime).Do(controllers.Run)

		_, t := sheduler.NextRun()

		select {
		case <-sheduler.Start():
		default:
			color.Green("Kidstaff Parser Sheduler Runned! Hello blo)))")
			color.Cyan("Next Parser Start at: %s", t)
		}

	} else {

		go controllers.Run()

	}

	xml_filename := controllers.ApplicationConfig.Xml.ReturnedXmlFileName
	delete_json_filename := controllers.ApplicationConfig.Xml.ReturnedDeleteJsonFileName
	srv := startHttpServer(":3000", xml_filename, delete_json_filename)
	defer func() {
		if err := srv.Shutdown(context.TODO()); err != nil {
			panic(err) // failure shutting down the server gracefully
		}
	}()

}
