package controllers

import (
	"github.com/parsers/kidstaff-parser/controllers/application"
	"github.com/parsers/kidstaff-parser/controllers/besplatka"
	"github.com/parsers/kidstaff-parser/controllers/kidstaff"
)

var (
	ApplicationConfig application.AppConfig
)

func Initialize() {
	application.Init(&ApplicationConfig)
	besplatka.Init(&ApplicationConfig)
	kidstaff.Init(&ApplicationConfig)
}

func Run() {
	kidstaff.Run()
}
