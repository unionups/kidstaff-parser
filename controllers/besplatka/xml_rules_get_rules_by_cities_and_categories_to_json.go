package besplatka

import (
	"encoding/json"
	"fmt"
	// xj "github.com/basgys/goxml2json"
	// "github.com/beevik/etree"
	// "github.com/fatih/color"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/queue"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"net/url"
	// "os"
	"strconv"
	"strings"
	"sync"
)

type RulesKey struct {
	CityId, CategoryId string
}

func xmlRulesGetRulesByRegionsAndCities() {

	c := BesplatkaCollector.Clone()
	// c.MaxDepth = 100
	// c.Async = true
	// c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 50})
	q, _ := queue.New(
		50,                    // Number of consumer threads
		database.RedisStorage, // Use default queue storage
	)
	// delete previous data from storage
	// database.RedisStorage.Clear()
	db := database.DBConnect
	db.Unscoped().Delete(&models.BesplatkaRule{})

	allCities := []models.BesplatkaCity{}
	allCategories := []models.BesplatkaCategory{}

	db.Find(&allCities)
	db.Where("last = ?", true).Find(&allCategories)

	///////
	var tree_path sync.Map

	///// rules collector
	var rules_collection_map sync.Map
	////
	var required_tags sync.Map

	////// callbacks
	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("X-Requested-With", "XMLHttpRequest")
	})

	c.OnResponse(func(r *colly.Response) {
		s := string(r.Body[:])
		ss, _ := strconv.Unquote(s)
		r.Body = []byte(ss)
	})

	c.OnHTML("div.rule", func(e *colly.HTMLElement) {
		ctx := e.Request.Ctx
		key := RulesKey{CityId: ctx.Get("city_id"), CategoryId: ctx.Get("category_id")}

		xml_rule_string := e.DOM.First().Text()

		xml_rule_string = strings.TrimSpace(xml_rule_string)

		/// check if required
		if strings.Contains(xml_rule_string, "*") {

			xml_rule_string = strings.TrimLeft(xml_rule_string, "* \n")

			ts := xml_rule_string
			i := strings.Index(ts, ">")
			ts = ts[1:i]
			vs, _ := required_tags.LoadOrStore(key, "")
			vs, _ = required_tags.Load(key)
			temp := vs.(string)
			temp += (ts + ",")
			required_tags.Store(key, temp)

		} else {
			tag := xml_rule_string
			i := strings.Index(tag, ">")
			tag = tag[1:i]

			if !strings.Contains(tag, "cena") && !strings.Contains(tag, "currency") {
				r := e.DOM.Find("div.propvalues").Text()
				i = strings.Index(r, "<")
				text := strings.TrimSpace(r[0:i])

				if text != "" {
					v, _ := rules_collection_map.LoadOrStore(key, map[string]string{})
					v, _ = rules_collection_map.Load(key)
					temp := v.(map[string]string)
					temp[tag] = text

					rules_collection_map.Store(key, temp)
				}
			}
		}

	})

	for _, cat := range allCategories {

		CategoryTreePath := cat.Name

		tid := cat.ParentCategoryId
		for tid != "" {
			temp_cat := models.BesplatkaCategory{}
			db.First(&temp_cat, "id_item = ?", tid)

			CategoryTreePath = strings.Join([]string{temp_cat.Name, CategoryTreePath}, "/")
			tid = temp_cat.ParentCategoryId
		}

		tree_path.Store(cat.IdItem, strings.Join([]string{"Детский мир", CategoryTreePath}, "/"))

		//////////////////////////

		// for _, city := range allCities {

		ctx := colly.NewContext()
		ctx.Put("category_id", cat.IdItem)
		ctx.Put("city_id", "303")

		u, _ := url.Parse("https://besplatka.ua/xmlcatrules")
		r_body := fmt.Sprintf("category_id=%s&city_id=%v", cat.IdItem, 303)

		q.AddRequest(&colly.Request{
			Method: "POST",
			URL:    u,
			Body:   strings.NewReader(r_body),
			Ctx:    ctx,
		})
	}

	q.Run(c)
	c.Wait()

	database.RedisStorage.Clear()

	var wait sync.WaitGroup
	// rules_collection_map.Range(func(key, rules_m interface{}) bool {
	// 	k := key.(RulesKey)
	// 	r_m := rules_m.(map[string]string)
	// 	wait.Add(1)

	// 	go func(key RulesKey, rule map[string]string) {
	// 		j, _ := json.Marshal(rule)
	// 		// color.Red("//////////////////////////////////////")
	// 		// color.Green(string(j))
	// 		rt, _ := required_tags.Load(key)
	// 		rs := strings.Split(rt.(string)[:len(rt.(string))-1], ",")
	// 		tp, _ := tree_path.Load(key.CategoryId)

	// 		besplatkaRule := models.BesplatkaRule{
	// 			CityId:           key.CityId,
	// 			CategoryId:       key.CategoryId,
	// 			CategoryTreePath: tp.(string),
	// 			RuleJSON:         []byte(j),
	// 			RequiredTags:     rs,
	// 		}
	// 		db.Create(&besplatkaRule)
	// 		wait.Done()
	// 	}(k, r_m)
	// 	return true
	// })
	required_tags.Range(func(key, req_tag interface{}) bool {
		k := key.(RulesKey)
		r_t := fmt.Sprintf("%v", req_tag)
		wait.Add(1)

		go func(key RulesKey, r_tag string) {
			j := []byte("{}")
			if r_m, ok := rules_collection_map.Load(key); ok {
				j, _ = json.Marshal(r_m.(map[string]string))
			}
			// color.Red("//////////////////////////////////////")
			// color.Green(string(j))
			rt := r_tag
			rs := strings.Split(rt[:len(rt)-1], ",")
			tp, _ := tree_path.Load(key.CategoryId)

			besplatkaRule := models.BesplatkaRule{
				CityId:           key.CityId,
				CategoryId:       key.CategoryId,
				CategoryTreePath: tp.(string),
				RuleJSON:         j,
				RequiredTags:     rs,
			}
			db.Create(&besplatkaRule)
			wait.Done()
		}(k, r_t)
		return true
	})
	wait.Wait()

}
