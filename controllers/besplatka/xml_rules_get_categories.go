package besplatka

import (
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	"github.com/gocolly/colly"
	"github.com/jinzhu/copier"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"log"
	"strconv"
	"strings"
)

func xmlRulesGetGetCategories() {
	c := BesplatkaCollector.Clone()

	c.MaxDepth = 4
	c.Async = true
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 4})

	db := database.DBConnect
	db.Unscoped().Delete(&models.BesplatkaCategory{})

	existed_categories_ids := make(map[string]struct{})
	allCategories := []models.BesplatkaCategory{}
	db.Find(&allCategories)

	for _, category := range allCategories {
		existed_categories_ids[category.IdItem] = struct{}{}
	}

	c.OnResponse(func(r *colly.Response) {
		data := []struct {
			Depth             string `json:"depth"`
			StringChildrenIds string `json:"children_ids"`
			T                 struct {
				IdItem string `json:"id_item"`
				Name   string `json:"name"`
			}
			Last bool `json:"last"`
		}{}

		err := json.Unmarshal(r.Body, &data)
		if err != nil {
			log.Fatal(err)
		}

		for _, el := range data {

			if (el.Depth == "2") && !stringInSlice(strings.Join([]string{"Детский мир", el.T.Name}, "/"), Config.AllowedCategoriesIds) {
				continue
			}

			var besplatkaCategory models.BesplatkaCategory

			if _, ok := existed_categories_ids[el.T.IdItem]; ok == false {

				for _, id := range strings.Split(el.StringChildrenIds, ",") {
					if id != el.T.IdItem {
						besplatkaCategory.ChildrenIds = append(besplatkaCategory.ChildrenIds, id)
					}
				}

				copier.Copy(&besplatkaCategory, &el)
				copier.Copy(&besplatkaCategory, &el.T)

				db.Create(&besplatkaCategory)

				existed_categories_ids[el.T.IdItem] = struct{}{}
				allCategories = append(allCategories, besplatkaCategory)

				color.Blue("Depth >> %s\n", besplatkaCategory.Depth)
				if besplatkaCategory.Last {
					continue
				}
				subcategories_url := fmt.Sprintf("/ajax/categories-by-parent?id=%s&show_count=&language=", besplatkaCategory.IdItem)
				color.Blue("Visit >> %s\n", subcategories_url)

				if e := r.Request.Visit(subcategories_url); e != nil {
					color.Red("Visit ERROR>> %s\n", e)
				}

			}

		}

	})

	c.Visit("https://besplatka.ua/ajax/categories-by-parent?id=582&show_count=&language=")
	c.Wait()
	for _, cat := range allCategories {
		if cat.Last != true {
			for _, chidId := range cat.ChildrenIds {
				color.Green("Debug >> chidId %s\n", chidId)
				if chidId != cat.IdItem {
					var besplatkaCategory models.BesplatkaCategory
					db.First(&besplatkaCategory, "id_item = ?", chidId)
					if s, err := strconv.Atoi(besplatkaCategory.Depth); err == nil {
						if s > 2 {
							db.Model(&besplatkaCategory).Update("parent_category_id", cat.IdItem)
						}
					}

				}
			}
		}
	}

}

// {"id":"588","depth":"2","children_ids":"588",
// "show_geo_address":"0","required_geo":"0","hide_in_create":"0",
// "t":{"id":"1691","id_item":"588","language":"ru","name":"Автокресла",
// 		"filter_text":"","filter_text_2":"","path_text":""},
// 		"url":"/detskiy-mir/avtokresla","last":true,"count":"9 190","counter":9190,"order":0}
