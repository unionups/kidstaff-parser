package besplatka

import (
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	// "github.com/gocolly/colly/debug"
	// "github.com/jinzhu/gorm"

	"github.com/parsers/kidstaff-parser/controllers/application"
	"github.com/parsers/kidstaff-parser/database"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
)

type BesplatkaConfig struct {
	AllowedCategoriesIds []string `yaml:"allowed_categories_ids"`
}

var (
	ApplicationConfig  *application.AppConfig
	Config             BesplatkaConfig
	BesplatkaCollector *colly.Collector
)

func Init(applicationConfig *application.AppConfig) {
	data, err := ioutil.ReadFile("config/besplatka.yml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err = yaml.Unmarshal([]byte(data), &Config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	ApplicationConfig = applicationConfig
	BesplatkaCollector = colly.NewCollector(
		colly.AllowedDomains(applicationConfig.Collector.Allowed.Domains...),
		colly.AllowURLRevisit(),
		// colly.Debugger(&debug.LogDebugger{}),
	)
	extensions.RandomUserAgent(BesplatkaCollector)
	// add storage to the collector
	err = BesplatkaCollector.SetStorage(database.RedisStorage)
	if err != nil {
		panic(err)
	}

	// delete previous data from storage
	if err = database.RedisStorage.Clear(); err != nil {
		log.Fatal(err)
	}

	xmlRulesGetRegionsAndCities()
	xmlRulesGetGetCategories()
	xmlRulesGetRulesByRegionsAndCities()
}

func stringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}
