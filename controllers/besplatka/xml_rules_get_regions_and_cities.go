package besplatka

import (
	"encoding/json"
	"github.com/gocolly/colly"
	"github.com/jinzhu/copier"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"log"
)

func xmlRulesGetRegionsAndCities() {
	c := BesplatkaCollector.Clone()
	c.Async = true
	c.OnResponse(func(r *colly.Response) {
		data := []struct {
			CityId    string `json:"id"`
			RegionId  string `json:"region_id"`
			Region    string `json:"region"`
			Name      string `json:"name"`
			RegCenter int    `json:"reg_center"`
		}{}

		err := json.Unmarshal(r.Body, &data)
		if err != nil {
			log.Fatal(err)
		}
		// log.Printf("%+v\n", data)
		db := database.DBConnect

		db.Unscoped().Delete(&models.BesplatkaRegion{})
		db.Unscoped().Delete(&models.BesplatkaCity{})

		existed_regions_ids, existed_cities_ids := make(map[string]struct{}), make(map[string]struct{})
		allRegions, allCities := []models.BesplatkaRegion{}, []models.BesplatkaCity{}

		db.Find(&allRegions)
		db.Find(&allCities)

		for _, region := range allRegions {
			existed_regions_ids[region.RegionId] = struct{}{}
		}
		for _, city := range allCities {
			existed_cities_ids[city.CityId] = struct{}{}
		}

		for _, el := range data {
			var besplatkaRegion models.BesplatkaRegion
			var besplatkaCity models.BesplatkaCity
			if _, ok := existed_regions_ids[el.RegionId]; ok == false {
				besplatkaRegion.RegionId = el.RegionId
				besplatkaRegion.Name = el.Region
				db.Create(&besplatkaRegion)
				existed_regions_ids[el.RegionId] = struct{}{}
			}

			if _, ok := existed_cities_ids[el.CityId]; ok == false {
				copier.Copy(&besplatkaCity, &el)
				db.Create(&besplatkaCity)
				existed_cities_ids[el.CityId] = struct{}{}
			}

		}

	})

	// c.Request("GET", "https://besplatka.ua/ajax/all-geografy?only_cities=1", nil, nil, nil)

	c.Visit("https://besplatka.ua/ajax/all-geografy?only_cities=1")
	c.Wait()
}

// only_cities=1
// [{"id":"1","region_id":"1","name":"Авдеевка","region":"Донецкая область","url":"/avdeevka",
// "lat":48.1366000000000013869794202037155628204345703125,
// "lng":37.7490999999999985448084771633148193359375,"reg_center":0},
