package by_category

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/beevik/etree"

	// "github.com/btubbs/datetime"
	"encoding/json"
	"fmt"

	"github.com/fatih/color"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"

	// "github.com/gocolly/colly/debug"
	"crypto/md5"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"os"

	"github.com/gocolly/colly/queue"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"github.com/parsers/kidstaff-parser/controllers/kidstaff/image_downloader"
	"gopkg.in/yaml.v3"

	// "regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

type KidstaffConfig struct {
	Limits               map[string]string                          `yaml:"limits"`
	AllowedCategoriesIds []string                                   `yaml:"allowed_categories_ids"`
	Categories           map[string]map[string]map[string]yaml.Node `yaml:"categories"`
	RazmerByAgeAndRost   map[string]string                          `yaml:"razmer_by_age_and_rost"`
	ImageSettings		 map[string]string							`yaml:"images"`
}

type xmlWriteData struct {
	xmlDoc   *etree.Document
	xmlModel *models.KidstaffProduct
}

var imageDownloader *image_downloader.RsyncSyncer

var allCities []models.KidstaffCity
var allCategories []models.KidstaffCategory
var allBesplatkaRules []models.BesplatkaRule
var allBesplatkaCities []models.BesplatkaCity
var allBesplatkaRegions []models.BesplatkaRegion

// var existedProductsIds []string
var assotiations_config_map KidstaffConfig //// <-------

///////////////////////////////////////////////////////////
//////////////////////////////////////////////////
////////////////////////////////////////////////

func KidsClothing(c *colly.Collector) {
	var fileMutex sync.RWMutex

	start_scan := time.Now()

	///////////////////////////
	/// Read kidstaff  config
	data, err := ioutil.ReadFile("config/kidstaff.yml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	err = yaml.Unmarshal([]byte(data), &assotiations_config_map)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	parralelizm_conf, _ := strconv.Atoi(assotiations_config_map.Limits["parallelism"])
	max_depth_conf, _ := strconv.Atoi(assotiations_config_map.Limits["max_depth"])

	c.MaxDepth = max_depth_conf
	c.Async = true
	c.AllowURLRevisit = true

	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: parralelizm_conf})
	sub_c := colly.NewCollector(
		// colly.Debugger(&debug.LogDebugger{}),
		colly.CacheDir("cache"),
		colly.AllowURLRevisit(),
	)
	sub_c.Async = true

	extensions.RandomUserAgent(c)
	extensions.RandomUserAgent(sub_c)
	extensions.Referer(c)
	extensions.Referer(sub_c)

	timeout, _ := strconv.Atoi(assotiations_config_map.ImageSettings["timeout"])

	imgSettings := image_downloader.ImageSyncerSettings{
		BaseDir:assotiations_config_map.ImageSettings["base_dir"],
		RemoteDir: assotiations_config_map.ImageSettings["remote_dir"],
		RemoteUser: assotiations_config_map.ImageSettings["remote_user"],
		RemoteHost: assotiations_config_map.ImageSettings["remote_host"],
		RemotePwd: assotiations_config_map.ImageSettings["remote_pwd"],
		Timeout: timeout}
	imageDownloader = image_downloader.NewRsyncSyncer(imgSettings)

	tag_assotiations_workers := map[string]worker{
		"sostoyanie":             sostoyanie,
		"razmer":                 razmer,
		"vid-obuvi":              vid_obuvi,
		"rost-sm":                rost_sm,
		"rost":                   rost_sm,
		"sezon":                  sezon,
		"pol":                    pol,
		"tip":                    tip,
		"vid":                    vid,
		"vid-izdeliya":           vid_izdeliya,
		"vid-yubki":              vid_yubki,
		"cvet":                   cvet,
		"proizvoditel":           proizvoditel,
		"proizvoditeli":          proizvoditeli,
		"material":               material,
		"niz":                    niz,
		"posadka":                exclude,
		"vozrastnaya-gruppa":     vozrastnaya_gruppa,
		"strana-izgotovleniya":   exclude,
		"strana-izgotovitel":     exclude,
		"pol-rebenka":            pol,
		"zastezhka":              exclude,
		"zastezhki":              exclude,
		"stil":                   exclude,
		"osobennosti-modeli":     exclude,
		"osobennosti":            exclude,
		"vid-uteplitelya":        vid_uteplitelya,
		"tip-uteplitelya":        vid_uteplitelya,
		"kapyushon":              exclude,
		"stil-odezhdy":           exclude,
		"fason-vyreza-gorloviny": exclude,
		"fason-rukava":           fason_rukava,
		"dlina-rukava":           fason_rukava,
		"vyrez":                  exclude,
		"vozrast":                vozrast,
		"siluet":                 exclude,
		"leginsy":                exclude,
		"dlinna-rukava":          exclude,
		"shtany":                 exclude,
		"yubka":                  exclude,
		"rukav":                  fason_rukava,
	}

	database.RedisStorage.Clear()
	gueue_threads, _ := strconv.Atoi(assotiations_config_map.Limits["queue_consumer_threads"])
	q, _ := queue.New(
		gueue_threads,         // Number of consumer threads
		database.RedisStorage, // Use default queue storage
	)

	db := database.DBConnect
	// db.Unscoped().Delete(&models.KidstaffCategory{})

	db.Find(&allCities)
	db.Find(&allCategories)
	db.Find(&allBesplatkaRules)
	db.Find(&allBesplatkaCities)
	db.Find(&allBesplatkaRegions)

	for_kiev_city_id := findBesplatkaCityIdByName("Киев")

	allBesplatkaCitiesById := make(map[string]models.BesplatkaCity)
	allBesplatkaRegionsById := make(map[string]models.BesplatkaRegion)
	allBesplatkaRulesById := make(map[string]models.BesplatkaRule)

	for _, v := range allBesplatkaCities {
		allBesplatkaCitiesById[v.CityId] = v
	}
	for _, v := range allBesplatkaRegions {
		allBesplatkaRegionsById[v.RegionId] = v
	}

	for _, rule := range allBesplatkaRules {

		str := strconv.FormatUint(uint64(rule.ID), 10)
		kiev_RuleJSON := findBesplatkaRuleJSONByCategotyIdAndCityId(for_kiev_city_id, rule.CategoryId)
		jsonMap := make(map[string]string)

		err := json.Unmarshal([]byte(kiev_RuleJSON), &jsonMap)
		if err != nil {
			panic(err)
		}
		rule.RuleMap = jsonMap

		allBesplatkaRulesById[str] = rule
	}

	///////////////////////
	//// concurrent xml writer
	/////////////////////
	d := time.Now().AddDate(0, 0, +1)
	year, month, day := d.Date()
	file_path := fmt.Sprintf("./XML_FILES/detskiy-mir-besplatka-kidstaff-%v-%v-%v.xml", year, month, day)

	xml_file, err := os.OpenFile(file_path, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_, err = io.Copy(xml_file, strings.NewReader("</realities>"))
		if err != nil {
			log.Fatal(err)
		}
		xml_file.Close()
	}()

	xml_beginer := strings.NewReader(fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<realities data-create=\"%s\">", time.Now().UTC().Format("2006-01-02T15:04:05+00:00")))
	_, err = io.Copy(xml_file, xml_beginer)
	if err != nil {
		log.Fatal(err)
	}

	xmlWrite := func(data xmlWriteData) {

		if res := db.Omit("url", "category").Save(&data.xmlModel); res.Error != nil {
			db.Unscoped().Omit("created_at", "url", "category").Save(&data.xmlModel)
			fileMutex.Lock()
			data.xmlDoc.WriteTo(xml_file)
			fileMutex.Unlock()

		} else {
			if data.xmlModel.CreatedAt.After(data.xmlModel.UpdatedAt) {
				fileMutex.Lock()
				data.xmlDoc.WriteTo(xml_file)
				fileMutex.Unlock()
			}
		}
	}

	/////////////////////////////////////////
	//////////// REQUESTS CALLBACKS
	//////////////////////////////////////
	// sub_c.OnRequest(func(r *colly.Request) {
	// 	color.Yellow("ON SUB_C Request>> %s\n", r.URL.String())

	// })

	// sub_c.OnResponse(func(r *colly.Response) {
	// 	color.Yellow("ON Response")
	// 	// s := string(r.Body[:])

	// })

	c.OnHTML(`div.ng-cards-wrapper div[class="ng-card"]`, func(e *colly.HTMLElement) {
		// if e.DOM.Find("div.premshop-link").Length() != 0 {
		// 	color.White("On HTML PREMSHOP BYPASS")
		// 	return
		// }
		color.Red("On HTML URL: %s", e.Request.URL.String())
		gq := e.DOM
		ctx := e.Request.Ctx

		title := gq.Find("div.card-title")
		title_text := title.Text()
		u, _ := title.Find("a[href]").Attr("href")
		cena_val := gq.Find("span.price-val").Text()
		if checkIfNotEmpty(title_text, u, cena_val) == false {
			return
		}

		color.Green("title: %s, url: %s, cena: %s\n ", title_text, u, cena_val)
		cx := colly.NewContext()
		cx.Put("rule_id", ctx.Get("rule_id"))
		cx.Put("becplatka_city_id", ctx.Get("becplatka_city_id"))
		cx.Put("title_text", title_text)
		cx.Put("u", u)
		cx.Put("cena_val", cena_val)
		cx.Put("product_id", e.Attr("id"))
		Url := "https://www.kidstaff.com.ua" + u
		sub_c.Request("GET", Url, nil, cx, nil)

	})

	////////////////////
	c.OnHTML("div#listalka a[title*='Следующая страница']", func(e *colly.HTMLElement) {
		color.Cyan("//////////////////////// ON NEXT PAGE URL: %s", e.Attr("href"))
		c.Visit(e.Attr("href"))
	})
	////////////////////////
	// sub_c.OnRequest(func(r *colly.Request) {
	// 	r.Headers.Set("X-Requested-With", "XMLHttpRequest")
	// })
	////////////////////////
	sub_c.OnResponse(func(r *colly.Response) {
		if len(r.Body) > 100 {
			return
		}
		rule := allBesplatkaRulesById[r.Ctx.Get("rule_id")]

		//////////////
		tel := string(r.Body[:])
		color.White(tel)

		i, _ := strconv.Atoi(r.Ctx.Get("product_id"))
		kidstaffProduct := models.KidstaffProduct{
			ID:       uint(i),
			URL:      "https://www.kidstaff.com.ua" + r.Ctx.Get("u"),
			Category: "detskiy-mir-besplatka-kidstaff.xml",
		}

		doc := etree.NewDocument()
		reality := doc.CreateElement("reality")
		reality.CreateElement("local_reality_id").CreateText(r.Ctx.Get("product_id"))
		city := allBesplatkaCitiesById[r.Ctx.Get("becplatka_city_id")]
		region := allBesplatkaRegionsById[city.RegionId].Name
		region = strings.TrimSuffix(region, "область")
		reality.CreateElement("state").CreateText(region)
		reality.CreateElement("city").CreateText(city.Name)
		reality.CreateElement("category").CreateText(rule.CategoryTreePath)
		title_text := html.EscapeString(r.Ctx.Get("title_text"))
		reality.CreateElement("title").CreateText(title_text)
		description_text := html.EscapeString(r.Ctx.Get("description"))
		reality.CreateElement("description").CreateText(description_text)
		reality.CreateElement("telephone").CreateText(tel)
		photos_urls := reality.CreateElement("photos_urls")
		imgs_str := r.Ctx.Get("imgs_str")
		imgs := strings.Split(imgs_str, ",")

		var img_str string
		for _, img := range imgs {
			photos_urls.CreateElement("loc").CreateText(img)
			img_str += img
			imageDownloader.SyncFile(img)
		}
		characteristics := reality.CreateElement("characteristics")

		for _, req_tag := range rule.RequiredTags {
			characteristics.CreateElement(req_tag).CreateText(r.Ctx.Get(req_tag))
		}

		for tag, _ := range rule.RuleMap {
			val := r.Ctx.Get(tag)
			if val != "" {
				characteristics.CreateElement(tag).CreateText(val)
			}
		}

		characteristics.CreateElement("cena").CreateText(r.Ctx.Get("cena_val"))
		characteristics.CreateElement("currency").CreateText("Гривна")
		tmp, _ := doc.WriteToString()
		reality.CreateElement("hash").CreateText(fmt.Sprintf("%x", md5.Sum([]byte(tmp))))
		reality.CreateElement("image_hash").CreateText(fmt.Sprintf("%x", md5.Sum([]byte(img_str))))

		doc.Indent(4)

		xmlWrite(xmlWriteData{xmlDoc: doc, xmlModel: &kidstaffProduct})
	})

	sub_c.OnHTML("article div.main-block-item span.showphone[id]", func(e *colly.HTMLElement) {
		color.Cyan("//////////////////////// ")
		color.Cyan("/// Tel. Find: %s", e.Attr("id"))
		color.Cyan("//////////////////////// ")
		color.Red("On HTML URL: %s", e.Request.URL.String())

		besplatkaRule := allBesplatkaRulesById[e.Request.Ctx.Get("rule_id")]
		color.Yellow("%v", besplatkaRule.CategoryTreePath)

		// v := ctx.Get("rule_id")
		// color.Red("On HTML URL CTX Rule ID: %s", v)
		// besplatkaRule := allBesplatkaRulesById[v]
		// // color.Red("On HTML URL CTX Rule Rule: %v", besplatkaRule)

		// color.Yellow("%v", besplatkaRule.CategoryTreePath)
		// // color.Yellow("%v", besplatkaRule)

		// cx := colly.NewContext()

		// e.Request.Ctx.Put("description", description)

		root := e.DOM.ParentsUntil("article div.main-block-item").Unwrap()

		color.Red("%v", besplatkaRule.RequiredTags)
		for _, req_tag := range besplatkaRule.RequiredTags {
			if tag_calback, exist := tag_assotiations_workers[req_tag]; exist {
				value := tag_calback(e)
				if checkIfNotEmpty(value) == false {
					return
				}
				e.Request.Ctx.Put(req_tag, value)
				color.Yellow("%s: %s", req_tag, value)

			} else {
				color.Red("///////////////////// IS Not Founded Worker for Required TAG: %s\n", req_tag)

			}

		}

		root = e.DOM.ParentsUntil("article div.main-block-item").Unwrap()

		cont := root.Find("div#newmaintextd > div:last-child")
		br := cont.Find("br")
		br.Each(func(_ int, s *goquery.Selection) {
			s.Parent().SetText("\n")
			s.Remove()
		})

		// description := cont.Text()
		cont.Find("a[href]").Parent().Remove()
		var sb strings.Builder
		// space := regexp.MustCompile(`\s+`)

		cont.Children().Each(func(_ int, el *goquery.Selection) {
			el_name := goquery.NodeName(el)
			if stringInSlice(el_name, []string{"div", "p", "span"}) {
				s := el.Text()
				if strings.Contains(s, "Фото") || strings.Contains(s, "фото") {
					return
				}
				// s = space.ReplaceAllString(s, " ")
				s = strings.Replace(s, "  ", "", -1)
				s = strings.TrimSpace(s)
				if s != "" {
					sb.WriteString(fmt.Sprintf("%s\n\n", s))
				}
			}

		})

		description := sb.String()
		description = strings.TrimSpace(description)
		if description == "" {
			description = strings.TrimSpace(cont.Text())
			description = strings.Replace(description, "  ", "", -1)
		}
		dl := len(description)
		if (dl < 2000) && (dl > 16) {

			description = html.EscapeString(description)
			color.Cyan(description)

			imgs := root.Find("a.groupics.cboxElement > img[src]")
			src_arr := []string{}
			imgs.Each(func(_ int, img *goquery.Selection) {
				src, _ := img.Attr("src")
				xi := strings.LastIndex(src, "x")

				if xi != -1 {
					src = strings.Replace(src, src[(xi-3):(xi+4)], "600x600", 1)
				}
				src_arr = append(src_arr, src)
			})
			imgs_str := strings.Join(src_arr, ",")

			for tag, rule := range besplatkaRule.RuleMap {

				if tag_calback, exist := tag_assotiations_workers[tag]; exist {
					value := tag_calback(e, rule)
					if checkIfNotEmpty(value) == true {
						e.Request.Ctx.Put(tag, value)
						color.Yellow("%s: %s", tag, value)
					}

				} else {
					color.Red("///////////////////// IS Not Founded Worker for TAG: %s\n", tag)

				}
			}

			e.Request.Ctx.Put("imgs_str", imgs_str)
			e.Request.Ctx.Put("description", description)
			// color.White(description)
			root_url := "https://www.kidstaff.com.ua/ajax/showmeuserphone.php"
			id := e.Attr("id")
			data := map[string]string{"suid": id}
			e.Request.Post(root_url, data)
		}
	})

	//////////////////////////////////////
	// Send subrequests to kidstaff (by cities and kidstaff categories)
	///////////////////////////////////////
	rules_map := getRulesMapFromConfig()

	for cat_url, rule := range rules_map {
		root_url := "https://www.kidstaff.com.ua"
		splits := strings.Split(rule, "|")
		subrequests := splits[1:]
		besplatka_cats := strings.Split(splits[0], "#")

		if len(subrequests) == 0 {
			root_url += "/goods"
		} else {
			root_url += "/search"
		}

		root_url += "/in-"

		for _, city := range allCities {
			//////// debug
			// var city models.KidstaffCity
			// for _, c := range allCities {
			// 	if c.Name == "Киев" {
			// 		city = c
			// 		break
			// 	}
			// }
			////////////////////////
			city_url := root_url + city.CityURL + cat_url

			besplatka_city_id := findBesplatkaCityIdByName(city.Name)
			besplatka_cat_path := make(map[string]string)
			if len(besplatka_cats) > 1 {

				for besplatka_cat_index, besplatka_cat := range besplatka_cats {
					subrequest := subrequests[besplatka_cat_index]
					/// get besplatka rule
					if (subrequest != "") && (besplatka_cat != "") {
						besplatka_cat_path[subrequest] = insertSubrequestToBesplatkaPath(besplatka_cat, subrequest)
					}

				}
			} else if len(subrequests) > 0 {
				for _, subrequest := range subrequests {
					/// get besplatka rule
					besplatka_cat_path[subrequest] = insertSubrequestToBesplatkaPath(besplatka_cats[0], subrequest)
				}
			} else {
				temp := strings.Split(besplatka_cats[0], "/")
				for k, v := range temp {
					temp[k] = strings.TrimSpace(v)
				}
				besplatka_cat_path["nosub"] = strings.Join(temp, "/")
			}

			for k, v := range besplatka_cat_path {
				br_pack := []models.BesplatkaRule{}
				for _, br := range allBesplatkaRules {
					if strings.TrimSpace(br.CategoryTreePath) == strings.TrimSpace(v) {
						if besplatka_city_id != "" {
							br_pack = append(br_pack, br)
						}
					}
				}

				var for_kiev_rule_id uint
				for _, br := range br_pack {
					if br.CityId == for_kiev_city_id {
						for_kiev_rule_id = br.ID
						break
					}
				}
				rule_id := for_kiev_rule_id
				for _, br := range br_pack {
					if strings.TrimSpace(br.CityId) == strings.TrimSpace(besplatka_city_id) {
						rule_id = br.ID
						break
					}
				}

				if besplatka_city_id != "" {
					if k == "nosub" {
						AddRequest(q, besplatka_city_id, rule_id, city_url)
					} else {
						req_url := city_url + subrequestReplacer(k)
						AddRequest(q, besplatka_city_id, rule_id, req_url)
					}

				}

			}
		}
		// q.Run(c)
		// c.Wait()
		// sub_c.Wait()
	}
	q.Run(c)
	c.Wait()
	sub_c.Wait()
	imageDownloader.Commit()
	var c1, c2 int
	db.Model(&models.KidstaffProduct{}).Where("created_at >= ?", start_scan).Count(&c1)
	db.Model(&models.KidstaffProduct{}).Where("updated_at >= ? AND created_at < ?", start_scan, start_scan).Count(&c2)
	color.Cyan("Parsing finished at %s\n", time.Now())
	color.Cyan("created count: %v\n", c1)
	color.Cyan("updated count: %v\n", c2)
}

func AddRequest(q *queue.Queue, c_id string, r_id uint, req_url string) {
	r_id_str := strconv.FormatUint(uint64(r_id), 10)
	ctx := colly.NewContext()
	ctx.Put("rule_id", r_id_str)
	ctx.Put("becplatka_city_id", c_id)
	u, _ := url.Parse(req_url)
	q.AddRequest(&colly.Request{
		Method: "GET",
		URL:    u,
		Ctx:    ctx,
	})
}

//https://www.kidstaff.com.ua/search/in-kiev/kids-clothing/shoes/pinetki/f-all-men-0-0_0-0

func getRulesMapFromConfig() map[string]string {

	type CatRule struct {
		CatGroupKey string
		CatKeys     map[string]string
	}

	var cat_rules_map []CatRule
	for _, allowed_root_cat := range assotiations_config_map.AllowedCategoriesIds {
		for root_cat_key, root_cat := range assotiations_config_map.Categories {
			if root_cat_key == allowed_root_cat {
				for cat_key, cat := range root_cat {
					color.Red("//////////////////////")
					color.Red("%s", cat_key)
					color.Red("//////////////////////")

					temp := make(map[string]string)
					for sub_cat_key, sub_cat := range cat {
						temp[sub_cat_key] = sub_cat.Value
						color.Red("%s", sub_cat_key)
						color.Yellow("%v\n", sub_cat.Value)
					}
					var cat_rule CatRule
					cat_rule.CatGroupKey = cat_key
					cat_rule.CatKeys = temp
					cat_rules_map = append(cat_rules_map, cat_rule)
				}
			}
		}
	}

	refactored_cat_name_to_id_rules_map := make(map[string]string)

	for _, cat_group := range cat_rules_map {

		var cat_group_id uint

		default_pre := cat_group.CatKeys["default_pre"]
		default_post := cat_group.CatKeys["default_post"]

		for _, c := range allCategories {
			if c.Name == cat_group.CatGroupKey {
				cat_group_id = c.ID
				break
			}
		}

		for cat, rule := range cat_group.CatKeys {
			if (cat != "default_pre") && (cat != "default_post") {

				for _, c := range allCategories {
					if strings.Contains(c.Name, cat[2:]) && (c.ParentCategoryId == cat_group_id) {
						var rs strings.Builder
						splits := strings.Split(rule, "#")

						if len(splits) > 1 {
							for key, r := range splits {
								if key > 0 {
									if spaceRemover(r) != "" {
										splits[key] = default_pre + " /" + r
									} else {
										splits[key] = ""
									}
								} else {
									splits[key] = default_pre + " " + r
								}

							}
							rs.WriteString(strings.Join(splits, "#"))

						} else {
							rs.WriteString(default_pre)
							rs.WriteString(rule)
						}

						rs.WriteString(default_post)

						refactored_cat_name_to_id_rules_map[c.Url] = rs.String()
						break
					}
				}
			}
		}
	}

	return refactored_cat_name_to_id_rules_map
}

//////////////////////////
