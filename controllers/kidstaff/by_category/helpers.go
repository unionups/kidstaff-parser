package by_category

import (
	"github.com/PuerkitoBio/goquery"
	// "github.com/fatih/color"
	"github.com/gocolly/colly"
	"strings"
)

////////////////////
func cardDetailsFinder(e *colly.HTMLElement, param string) []string {
	root := e.DOM.ParentsUntil("div.main-block-item").Unwrap()
	ul := root.Find("div.tth-tovar ul")
	if len(ul.Nodes) == 0 {
		if strings.Contains(e.Request.URL.String(), "tema") {
			root := e.DOM.ParentsUntil("~").Unwrap()
			ul = root.Find("section div#tth-tovar ul")
		}
	}
	str := []string{}
	ul.EachWithBreak(func(_ int, el *goquery.Selection) bool {
		text := el.Text()
		if strings.Contains(text, param) {
			li := el.Find("li")
			li.Each(func(_ int, l *goquery.Selection) {
				t := l.Text()
				if strings.Contains(t, param) == false {
					str = append(str, strings.TrimSpace(t))
				}
			})
			return false
		}
		return true
	})
	return str
}

//////////////////////
func fetchCategoryFromUrl(Url string) string {
	url_temp_arr := strings.Split(Url, "/")
	kidstaff_kat := url_temp_arr[len(url_temp_arr)-1]
	if strings.Contains(kidstaff_kat, "0") {
		if strings.Contains(kidstaff_kat, "page") {
			kidstaff_kat = url_temp_arr[len(url_temp_arr)-3]
		} else {
			kidstaff_kat = url_temp_arr[len(url_temp_arr)-2]
		}
	} else {
		if strings.Contains(kidstaff_kat, "page") {
			kidstaff_kat = url_temp_arr[len(url_temp_arr)-2]
		}
	}

	return kidstaff_kat
}

//////////////////////
func fetchCategoryFromKroshki(e *colly.HTMLElement) string {
	// root := e.DOM.ParentsUntil("~").Unwrap()
	root := e.DOM.ParentsUntil("article").Unwrap()
	u, _ := root.Find("div.kroshki span:last-child a").Last().Attr("href")
	return fetchCategoryFromUrl(u)
}

///////////////
func insertSubrequestToBesplatkaPath(besplatca_cat string, subrequest string) string {
	bc_path_splitted := strings.Split(besplatca_cat, "/")
	temp := []string{}
	temp = append(temp, bc_path_splitted[:2]...)
	temp = append(temp, subrequest)
	bc_path_splitted = append(temp, bc_path_splitted[2:]...)
	for key, el := range bc_path_splitted {
		bc_path_splitted[key] = strings.TrimSpace(el)
	}
	return strings.Join(bc_path_splitted, "/")
}

/////////////////////
func subrequestReplacer(s string) string {
	s = strings.TrimSpace(s)
	switch s {
	case "Для девочек":
		return "/f-all-women-0-0_0-0"
	case "Для мальчиков":
		return "/f-all-men-0-0_0-0"
	}
	return ""
}

///////////////////
func checkIfNotEmpty(args ...string) bool {
	for _, s := range args {
		if s == "" {
			return false
		}
	}
	return true
}

/////////////////////////////
func findBesplatkaCityIdByName(name string) string {
	for _, city := range allBesplatkaCities {
		if strings.TrimSpace(city.Name) == strings.TrimSpace(name) {
			return city.CityId
		}
	}
	return ""
}

/////////////////////////////////
func findBesplatkaRuleJSONByCategotyIdAndCityId(city_id string, cat_id string) []byte {
	for _, rule := range allBesplatkaRules {
		if (rule.CityId == city_id) && (rule.CategoryId == cat_id) {
			return rule.RuleJSON
		}
	}
	return []byte("")
}

///////////
func spaceRemover(str string) string {
	return strings.Join(strings.Fields(str), "")
}

func stringInSlice(str string, list []string) bool {
	for _, v := range list {
		if strings.TrimSpace(v) == strings.TrimSpace(str) {
			return true
		}
	}
	return false
}

func custom_mapper(e *colly.HTMLElement, m map[string]string, details string) string {
	str_m := cardDetailsFinder(e, details)
	if len(str_m) != 0 {
		temp := []string{}
		for _, v := range str_m {
			v := strings.TrimSpace(v)
			if m[v] != "" {
				temp = append(temp, m[v])
			}
		}
		return strings.Join(temp, ",")
	}
	return ""

}

func custom_finder(e *colly.HTMLElement, variants, details, defaults string) string {
	str_m := cardDetailsFinder(e, details)
	t := []string{}
	if strings.TrimSpace(variants) != "" {
		values := strings.Split(variants, ",")
		for _, s := range str_m {
			for _, v := range values {
				if strings.EqualFold(spaceRemover(v), spaceRemover(s)) {
					t = append(t, strings.TrimSpace(v))
				}
			}
		}
		text := strings.Join(t, ",")
		if text != "" {
			return text
		}
	}
	return defaults
}
