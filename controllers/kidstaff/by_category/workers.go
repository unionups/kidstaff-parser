package by_category

import (
	// "github.com/PuerkitoBio/goquery"
	// "github.com/fatih/color"
	"github.com/gocolly/colly"
	"regexp"
	"strconv"
	"strings"
)

////// tags workers
type worker func(*colly.HTMLElement, ...string) string

//// sostoyanie tag
func sostoyanie(e *colly.HTMLElement, variants ...string) string {
	root := e.DOM.ParentsUntil("div.main-block-item").Unwrap()
	txt := root.Find("div.itemsost").Text()
	fi := strings.Fields(txt)
	if len(fi) != 0 {
		txt = strings.TrimSpace(fi[0])
		switch txt {
		case "Б/У":
			return "Б/у"
		case "Новое":
			return "Новый"
		}
	}
	return ""
}

//// razmer tag
func razmer(e *colly.HTMLElement, variants ...string) string {
	razmer_assotiations := assotiations_config_map.RazmerByAgeAndRost
	str := cardDetailsFinder(e, "Размер")
	if len(str) == 0 {
		str = cardDetailsFinder(e, "Возраст")
		if len(str) == 0 {
			str = cardDetailsFinder(e, "Рост")
			if len(str) == 0 {
				str = cardDetailsFinder(e, "Стелька (см)")
			}
		}
	} else {
		return strings.Join(str, ",")
	}

	if len(str) != 0 {

		besplatka_vals := []string{}
		for _, s := range str {
			key := spaceRemover(s)
			match, _ := regexp.MatchString(`\d{0,2}(.+\d{1,2})$`, key)
			if match {
				besplatka_vals = append(besplatka_vals, s)
			} else {
				ass := razmer_assotiations[key]
				if ass != "" {
					besplatka_vals = append(besplatka_vals, razmer_assotiations[key])
				}
			}
		}
		if len(besplatka_vals) != 0 {
			return strings.Join(besplatka_vals, ",")
		}
		return strings.Join(str, ",")
	} else {
		m := map[string]string{"pelenki-polotenca": "Разный", "konverty": "0-3 мес",
			"aksessuary": "Разные", "696": "Разный", "727": "Разный", "ukrashenija": "Разный",
			"podtjazhki-remni-pojasa": "Разный", "karnaval": "Разный",
			"737": "Разный", "nizhnee-bele-kupalniki-halaty": "Разный", "berety": "Разный", "1556": "Разный"}

		// c := fetchCategoryFromUrl(e.Request.URL.String())
		c := fetchCategoryFromKroshki(e)
		if m[c] != "" {
			return m[c]
		} else {
			return "Разный"
		}

	}

	return ""

}

/////// vid-obuvi
func vid_obuvi(e *colly.HTMLElement, variants ...string) string {
	kidstaff_cat_map := map[string]string{"pinetki": "Другой вид обуви", "tufli": "Туфли", "mokasiny": "Мокасины", "baletki-lodochki": "Балетки", "bosonozhki-sandalii": "Босоножки",
		"shlepki-vetnamki": "Тапочки", "botinki": "Ботинки",
		"sapogi": "Сапоги", "sapogi-rezinovye": "Сапоги", "uggi-snoubutsy": "Угги",
		"kedy-krossovki": "Кеды, Кроссовки",
		"tapochki":       "Тапочки", "Бутсы": "Бутсы"}
	// c := fetchCategoryFromUrl(e.Request.URL.String())
	c := fetchCategoryFromKroshki(e)
	vid := kidstaff_cat_map[c]
	if vid == "" {
		str := cardDetailsFinder(e, "Тип")
		if len(str) == 0 {
			return "Другой вид обуви"
		}
		vals := []string{}
		for _, s := range str {
			vals = append(vals, kidstaff_cat_map[s])
		}
		if len(vals) != 0 {
			return strings.Join(vals, ",")
		} else {
			return "Другой вид обуви"
		}

	}
	return vid
}

///// sezon
func sezon(e *colly.HTMLElement, variants ...string) string {
	str := cardDetailsFinder(e, "Сезон")
	if len(str) == 0 {
		str = cardDetailsFinder(e, "Сезонность")
	}
	if len(str) != 0 {
		if strings.EqualFold(strings.TrimSpace(str[0]), "Деми") {
			str[0] = "Демисезон"
		}
		return strings.Replace(strings.Join(str, ","), "-", "", 1)
	} else {
		return "Демисезон"
	}

}

///// tip
func tip(e *colly.HTMLElement, variants ...string) string {
	kidstaff_cat_map := map[string]string{"Бодик": "Боди и песочники", "Песочники": "Боди и песочники",
		"Распашонки": "Распашонка", "Человечки": "Ползунки", "Комплект": "Комплект, костюм",
		"Крыжма": "Другой тип одежды", "Набор для фотосессии": "Другой тип одежды",
		"Носочки": "Другой тип одежды", "Слюнявки": "Другой тип одежды", "Чепчики": "Чепчики, шапочки",
		"Шапочки": "Чепчики, шапочки", "komplekty": "Комплект, костюм", "konverty": "Другой тип одежды",
		"shuby-dublenki": "Натуральные, Искусственные", "Майки": "Майка", "Трусы": "Трусы",
		"noski": "Носки", "Платья": "Платье", "Сарафаны": "Сарафаны", "platja-sarafany-tuniki": "Платье",
		"sportivnaja-forma": "Спортивные костюмы", "plavanie": "Спортивные костюмы", "golfy": "Носки",
		"737": "Колготки, Носки", "halaty": "Халат", "pizhamy-nochnushki": "Пижама",
		"Купальник сдельн.": "Плавки", "Плавки+лиф": "Комплект белья, Плавки",
		"kupalniki-plavki": "Плавки", "golfy-vodolazki": "Водолазка", "kofty-kardigany-bolero": "Кофта",
		"Толстовки с капюшоном": "Толстовка", "reglany-tolstovki-svitera": "Реглан, Толстовка"}

	// c := fetchCategoryFromUrl(e.Request.URL.String())
	c := fetchCategoryFromKroshki(e)
	if stringInSlice(c, []string{"sportivnaja-forma", "tancy-gimnastika", "forma", "edinoborstva", "plavanie", "1632"}) {
		m := map[string]string{"Брюки": " Спортивные штаны", "Комплект": "Спортивные костюмы",
			"Кофта, топ": "Спортивные кофты", "Купальник": "Спортивные костюмы",
			"Лосины/бриджи": "Спортивные штаны", "Платье": "Спортивные костюмы", "Рубашки": "Спортивные костюмы",
			"Кимоно": "Спортивные костюмы", "Кофты": "Спортивные кофты", "Штаны": "Спортивные штаны",
			"Кофты, лонгсливы": "Спортивные кофты", "Футболки": " Спортивные футболки и майки",
			"Шорты": "Спортивные штаны"}
		maper := custom_mapper(e, m, "Тип одежды")
		if maper != "" {
			return maper
		}
		return "Спортивные костюмы"
	}

	if stringInSlice(c, []string{"kolgotki"}) {
		return "Колготки"
	}

	str_m := cardDetailsFinder(e, "Тип")
	var txt string
	if len(str_m) != 0 {
		temp := []string{}
		for _, v := range str_m {
			v := strings.TrimSpace(v)
			temp = append(temp, kidstaff_cat_map[v])
		}

		if temp[0] != "" {
			txt = strings.Join(temp, ",")
		} else {
			txt = strings.Join(str_m, ",")
		}
	} else {
		txt = kidstaff_cat_map[c]
	}
	return txt
}

////// vid
func vid(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"Полукомбинезоны": "Полукомбинезон", "Рубашка": "Рубашка"}
	mapper := custom_mapper(e, m, "Тип")

	if mapper != "" {
		return mapper

	} else {
		m := map[string]string{"kombinezony": "Комбинезон", "polukombinezony": "Полукомбинезон", "kombinezony-rompery": "Комбинезон",
			"zhiletki": "Классический жилет", "shljapki": "Шляпы, козырьки и кепки", "berety": "Береты", "shapki": " Шапки",
			"komplekty-golovnyh-uborov": "Наборы", "panamy-kosynki": "Шапки",
			"kepki-bejsbolki": "Шляпы, козырьки и кепки", "varezhki-perchatki": "Перчатки",
			"shlemy": "Шарфы-шапки", "sharfy": "Шарфы", "shtany-brjuki": "Штаны", "dzhinsy": "Джинсы",
			"shorty": "Шорты", "losiny-legginsy": "Классические леггинсы", "bridzhi-kapri": "Бриджи, Капри",
			"rubashki": "Рубашка", "bluzki": "Блузка", "vyshivanki": "Рубашка", "pidzhaki": "Пиджак", "zakolki": "Галстук с застежкой, Галстук самовяз, Галстук-бабочка"}
		// c := fetchCategoryFromUrl(e.Request.URL.String())
		c := fetchCategoryFromKroshki(e)
		return m[c]
	}
	return ""
}

///// vid-izdeliya
func vid_izdeliya(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"futbolki": "Футболка", "futbolki-majki": "Футболка, Майка",
		"majki": "Майка", "topy": "Топ"}
	// c := fetchCategoryFromUrl(e.Request.URL.String())
	c := fetchCategoryFromKroshki(e)
	return m[c]
}

func vid_yubki(e *colly.HTMLElement, variants ...string) string {
	str_m := cardDetailsFinder(e, "Длина")
	if len(str_m) != 0 {
		return strings.Join(str_m, ",")
	} else {
		return "Миди"
	}
}

//// rost-sm
func rost_sm(e *colly.HTMLElement, variants ...string) string {
	str_m := cardDetailsFinder(e, "Рост")
	return strings.Join(str_m, ",")
}

//// vozrast
func vozrast(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"18 - 24 мес": "1,5-3 года", "2 - 3 года": "1,5-3 года",
		"3 - 5 лет": "3-5 лет", "5 - 7 лет": "6-8 лет", "7 - 9 лет": "6-8 лет",
		"9 - 12 лет": "9-12 лет", "12 - 14 лет": "13 лет и более", "14 - 16 лет": "13 лет и более"}
	mapper := custom_mapper(e, m, "Возраст")
	if mapper != "" {
		return mapper
	}
	return vozrastnaya_gruppa(e, variants[0])

}

////cvet
func cvet(e *colly.HTMLElement, variants ...string) string {
	str_m := cardDetailsFinder(e, "Цвет")

	t := []string{}
	for _, v := range str_m {
		if strings.Contains(variants[0], v) {
			t = append(t, v)
		}
	}
	return strings.Join(t, ",")
}

////proizvoditel
func proizvoditel(e *colly.HTMLElement, variants ...string) string {
	return custom_finder(e, variants[0], "Торговая марка", "Другой производитель")
}

////proizvoditeli
func proizvoditeli(e *colly.HTMLElement, variants ...string) string {
	return proizvoditel(e, variants[0])
}

////material
func material(e *colly.HTMLElement, variants ...string) string {
	return custom_finder(e, variants[0], "Ткань", "Другой")
}

///vid-uteplitelya
func vid_uteplitelya(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"Slimtex": "Слимтекс", "Полиэстер": "Полиэстр",
		"Пух": "Пух", "Синтепон": "Синтепон", "Холлофайбер": "Холлофайбер"}
	return custom_mapper(e, m, "Наполнитель")
}

/////fason-rukava
func fason_rukava(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"3/4": "Три четверти", "Без рукава": "Короткий",
		"Длинный": "Длинный", "Короткий": "Короткий"}
	return custom_mapper(e, m, "Рукав")
}

/// vozrastnaya-gruppa
func vozrastnaya_gruppa(e *colly.HTMLElement, variants ...string) string {
	m := map[string]bool{}
	str := cardDetailsFinder(e, "Размер")
	if len(str) == 0 {
		str = strings.Split(razmer(e, variants[0]), ",")
	}
	for _, v := range str {
		if vv, err := strconv.Atoi(strings.Split(v, ",")[0]); err != nil {
		} else {
			switch {
			case (vv <= 28):
				m["1,5-3 года"] = true
			case (vv > 28) && (vv <= 32):
				m["3-5 лет"] = true
			case (vv > 32) && (vv <= 35):
				m["6-8 лет"] = true
			case (vv > 35) && (vv <= 38):
				m["9-12 лет"] = true
			case (vv > 38):
				m["13 лет и более"] = true
			}
		}
	}
	s := []string{}
	for k, v := range m {
		if v {
			s = append(s, k)
		}
	}
	return strings.Join(s, ",")
}

///// pol
func pol(e *colly.HTMLElement, variants ...string) string {
	txt := e.DOM.Find("div.card-condition").Text()
	txt = strings.TrimLeft(txt, "Товар:")
	t := strings.Split(txt, "|")
	switch strings.TrimSpace(t[len(t)-1]) {
	case "Для девочки":
		txt = "Для девочек"
	case "Для мальчика":
		txt = "Для мальчиков"
	default:
		txt = "Унисекс"
	}
	return txt
}

/// niz
func niz(e *colly.HTMLElement, variants ...string) string {
	m := map[string]string{"Ромпер": "Шорты", "Поддева": "Брюки", "Слип": "Брюки"}
	exclude := []string{"polukombinezony"}

	c := fetchCategoryFromKroshki(e)

	if stringInSlice(c, exclude) {
		return ""
	}

	mapper := custom_mapper(e, m, "Тип")
	if mapper != "" {
		return mapper
	}

	str_m := cardDetailsFinder(e, "Тип")

	for _, v := range str_m {
		if c == "kombinezony" && v == "Штаны" {
			return "Брюки"
		}
	}
	return ""
}

//////////////////////////////// mayby only exclude
///exclude
func exclude(e *colly.HTMLElement, variants ...string) string {
	return ""
}

/// posadka
// func posadka(e *colly.HTMLElement, variants ...string) string {
// 	exclude := []string{"dzhinsy", "shtany-brjuki"}
// 	c := fetchCategoryFromKroshki(e)
// 	if stringInSlice(c, exclude) {
// 		color.White("Find Excluded tag: posadka")
// 		return ""
// 	}
// 	color.White("Tag assotiations not found: posadka")
// 	return ""
// }

// ///// zastezhka
// func zastezhka(e *colly.HTMLElement, variants ...string) string {
// 	exclude := []string{"letnie-kurtki"}
// 	c := fetchCategoryFromKroshki(e)
// 	if stringInSlice(c, exclude) {
// 		color.White("Find Excluded tag: zastezhka")
// 		return ""
// 	}
// 	color.White("Tag assotiations not found: zastezhka")
// 	return ""
// }

// ///osobennosti-modeli
// func osobennosti_modeli(e *colly.HTMLElement, variants ...string) string {
// 	exclude := []string{"letnie-kurtki"}
// 	c := fetchCategoryFromKroshki(e)
// 	if stringInSlice(c, exclude) {
// 		color.White("Find Excluded tag: osobennosti-modeli")
// 		return ""
// 	}
// 	color.White("Tag assotiations not found: osobennosti-modeli")
// 	return ""
// }

// ///stil
// func stil(e *colly.HTMLElement, variants ...string) string {
// 	exclude := []string{"letnie-kurtki"}
// 	c := fetchCategoryFromKroshki(e)
// 	if stringInSlice(c, exclude) {
// 		color.White("Find Excluded tag: stil")
// 		return ""
// 	}
// 	color.White("Tag assotiations not found: stil")
// 	return ""
// }

// /////////// strana-izgotovleniya
// func strana_izgotovleniya(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: strana-izgotovleniya")
// 	return ""
// }

// /////////// kapyushon
// func kapyushon(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: kapyushon")
// 	return ""
// }

// /////////// stil-odezhdy
// func stil_odezhdy(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: stil-odezhdy")
// 	return ""
// }

///fason-vyreza-gorloviny
// func fason_vyreza_gorloviny(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: fason-vyreza-gorloviny")
// 	return ""
// }

// /////// vyrez
// func vyrez(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: vyrez")
// 	return ""
// }

// ////////siluet
// func siluet(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: siluet")
// 	return ""
// }

// //////// leginsy
// func leginsy(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: leginsy")
// 	return ""
// }

// ////////dlinna-rukava
// func dlinna_rukava(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: dlinna-rukava")
// 	return ""
// }

// ////////shtany
// func shtany(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: shtany")
// 	return ""
// }

// ////yubka
// func yubka(e *colly.HTMLElement, variants ...string) string {
// 	color.White("Find Excluded tag: yubka")
// 	return ""
// }
