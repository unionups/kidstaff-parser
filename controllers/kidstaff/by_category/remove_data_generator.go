package by_category

import (
	"github.com/fatih/color"
	// "github.com/jinzhu/gorm"
	// "encoding/json"
	"fmt"
	"github.com/gocolly/colly"
	// "github.com/gocolly/colly/debug"
	"github.com/gocolly/colly/queue"
	// "github.com/json-iterator/go"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"gopkg.in/yaml.v3"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

func RemoveDataGenerator(wg *sync.WaitGroup) {
	var config_map KidstaffConfig
	data, _ := ioutil.ReadFile("config/kidstaff.yml")
	yaml.Unmarshal([]byte(data), &config_map)

	var fileMutex sync.RWMutex
	start_scan := time.Now()
	color.Cyan("START DELETE and GENERATE DEL JSON.... ")
	db := database.DBConnect

	c := colly.NewCollector(
		// colly.Debugger(&debug.LogDebugger{}),
		colly.CacheDir("cache"),
	)

	gueue_threads, _ := strconv.Atoi(config_map.Limits["remove_data_generator_queue_consumer_threads"])
	q, _ := queue.New(
		gueue_threads, // Number of consumer threads
		&queue.InMemoryQueueStorage{MaxSize: 100000}, // Use default queue storage
	)

	for_check_models := []models.KidstaffProduct{}

	db.Omit("category", "created_at").Find(&for_check_models, "updated_at < ?", start_scan)

	d := time.Now().AddDate(0, 0, +1)
	year, month, day := d.Date()
	file_path := fmt.Sprintf("./XML_FILES/DELETE_JSON_FILES/delete-%v-%v-%v.json", year, month, day)

	json_file, err := os.OpenFile(file_path, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}

	_, err = io.Copy(json_file, strings.NewReader("[{\"filename\":\"detskiy-mir-X-kidstaff.xml\",\"delete_id\":["))
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_, err = io.Copy(json_file, strings.NewReader("]}]"))
		if err != nil {
			log.Fatal(err)
		}
		json_file.Close()
	}()

	isFirst := true
	SoftRemove := func(id_s string) {
		if id, err := strconv.Atoi(id_s); err != nil {
			log.Fatal(err)
		} else {
			db.Delete(&models.KidstaffProduct{ID: uint(id)})
			if !isFirst {
				id_s = "," + id_s
			} else {
				isFirst = false
			}
			fileMutex.Lock()
			_, err = io.Copy(json_file, strings.NewReader(id_s))
			fileMutex.Unlock()
			if err != nil {
				log.Fatal(err)
			}

		}
		wg.Done()
	}

	c.OnResponse(func(r *colly.Response) {
		if r.StatusCode == 404 {
			log.Println("On 404 find")
			wg.Add(1)
			go SoftRemove(r.Ctx.Get("model_id"))
			r.Request.Abort()
		}
	})

	c.OnHTML("div.main-block-item > div[align] > div.sechoyspno:not(:empty)", func(e *colly.HTMLElement) {
		// if e.Attr style="display:none;"
		// t := e.DOM.Text()
		// log.Printf("On HTML text: %s\n", t)
		// ch_strs := []string{"отключено", "продан", "помещено в корзину"}
		// for _, v := range ch_strs {
		// 	if strings.Contains(t, v) {
		// 		log.Printf("On HTML find: %s\n", v)
		wg.Add(1)
		go SoftRemove(e.Request.Ctx.Get("model_id"))
		e.Request.Abort()
		// break
		// 	}
		// }
	})

	for _, m := range for_check_models {
		ctx := colly.NewContext()
		ctx.Put("model_id", strconv.Itoa(int(m.ID)))
		u, _ := url.Parse(m.URL)
		q.AddRequest(&colly.Request{
			Method: "GET",
			URL:    u,
			Ctx:    ctx,
		})
	}

	q.Run(c)
	var count int
	db.Model(&models.KidstaffProduct{}).Unscoped().Where("deleted_at > ? ", start_scan).Count(&count)
	color.Green("DELETE and GENERATE DEL JSON Finished!! at: %s\n ", time.Now())
	color.Cyan("DELETEd count is: %v\n", count)
	wg.Done()
}

// https://www.kidstaff.com.ua/tema-28608484.html
// http://prntscr.com/o6iyyp
// http://prntscr.com/o6j1vf
// http://prntscr.com/o6j59k
