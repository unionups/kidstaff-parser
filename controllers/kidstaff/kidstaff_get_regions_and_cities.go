package kidstaff

import (
	// "encoding/json"
	"github.com/gocolly/colly"
	// "fmt"
	// "github.com/fatih/color"
	// "github.com/jinzhu/copier"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	// "log"
	// "strings"
)

func kidstaffGetRegionsAndCities() {

	c := KidstaffCollector.Clone() // Regions collector
	c.MaxDepth = 1
	c.Async = true
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 10})
	cc := KidstaffCollector.Clone() // Sities collector

	db := database.DBConnect

	db.Unscoped().Delete(&models.KidstaffCity{})
	db.Unscoped().Delete(&models.KidstaffRegion{})

	// cc.OnRequest(func(r *colly.Request) {
	// 	color.Cyan("Visiting", r.URL.String())
	// })
	// cc.OnResponse(func(r *colly.Response) {
	// 	color.Blue("%s\n", r.Body)
	// })

	allRegions := []models.KidstaffRegion{}
	allCities := []models.KidstaffCity{}
	existed_regions_ids := make(map[string]struct{})
	existed_cities_ids := make(map[string]struct{})
	db.Find(&allRegions)
	db.Find(&allCities)

	for _, region := range allRegions {
		existed_regions_ids[region.RegionId] = struct{}{}
	}

	for _, city := range allCities {
		existed_cities_ids[city.CityId] = struct{}{}
	}

	c.OnHTML("ul.bc-r-oblast a[data-regionid]", func(e *colly.HTMLElement) {

		var kidstaffRegion models.KidstaffRegion

		kidstaffRegion.RegionId = e.Attr("data-regionid")
		kidstaffRegion.Name = e.Text

		url := "https://www.kidstaff.com.ua/ajax/cities.php"
		data := map[string]string{"mode": "region_filter", "id": kidstaffRegion.RegionId}
		cc.Post(url, data)

		if _, ok := existed_regions_ids[kidstaffRegion.RegionId]; ok == false {
			db.Create(&kidstaffRegion)
			existed_regions_ids[kidstaffRegion.RegionId] = struct{}{}

			// allRegions = append(allRegions, kidstaffRegion)
		} else {
			kr := models.KidstaffRegion{}
			db.First(&kr, "region_id = ?", kidstaffRegion.RegionId)
			if kr.Name != kidstaffRegion.Name {
				kr.Name = kidstaffRegion.Name
				db.Save(&kr)
			}

		}

	})

	cc.OnHTML("ul.bc-r-city a[data-cityid]", func(e *colly.HTMLElement) {

		var kidstaffCity models.KidstaffCity

		kidstaffCity.CityId = e.Attr("data-cityid")
		kidstaffCity.CityURL = e.Attr("data-cityurl")
		kidstaffCity.RegionId = e.Attr("data-regionid")
		kidstaffCity.Name = e.Text

		if _, ok := existed_cities_ids[kidstaffCity.CityId]; ok == false {
			db.Create(&kidstaffCity)
			existed_cities_ids[kidstaffCity.CityId] = struct{}{}
			// allCities = append(allCities, kidstaffCity)
		} else {
			kc := models.KidstaffCity{}
			db.First(&kc, "city_id = ?", kidstaffCity.CityId)
			if (kc.CityURL != kidstaffCity.CityURL) || (kc.RegionId != kidstaffCity.RegionId) || (kc.Name != kidstaffCity.Name) {
				kc.Name = kidstaffCity.Name
				kc.CityURL = kidstaffCity.CityURL
				kc.RegionId = kidstaffCity.RegionId
				db.Save(&kc)
			}

		}

	})

	c.Visit("https://www.kidstaff.com.ua/goods/kids-clothing")

	c.Wait()
	cc.Wait()

	// for _, region := range allRegions {
	// 	if db.NewRecord(region) {
	// 		db.Create(&region)
	// 	} else {
	// 		kr := models.KidstaffRegion{}
	// 		db.First(&kr, "region_id = ?", region.RegionId)
	// 		if kr.Name == region.Name {
	// 			continue
	// 		}
	// 		kr.Name = region.Name
	// 		db.Save(&kr)

	// 	}
	// }

	// for _, city := range allCities {
	// 	if db.NewRecord(city) {
	// 		db.Create(&city)
	// 	} else {
	// 		kc := models.KidstaffCity{}
	// 		db.First(&kc, "city_id = ?", city.CityId)
	// 		if (kc.CityURL == city.CityURL) && (kc.RegionId == city.RegionId) && (kc.Name == city.Name) {
	// 			continue
	// 		}
	// 		kc.Name = city.Name
	// 		kc.CityURL = city.CityURL
	// 		kc.RegionId = city.RegionId
	// 		db.Save(&kc)
	// 	}
	// }

}
