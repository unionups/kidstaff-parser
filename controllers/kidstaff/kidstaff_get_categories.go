package kidstaff

import (
	"github.com/PuerkitoBio/goquery"
	// "github.com/fatih/color"
	"github.com/gocolly/colly"
	"github.com/parsers/kidstaff-parser/database"
	"github.com/parsers/kidstaff-parser/models"
	"strings"
	"sync"
)

func trim_goods(url string, b ...bool) string {
	return strings.TrimPrefix(url, "/goods")
}

func kidstaffGetCategories() {

	c := KidstaffCollector.Clone() // collector
	c.MaxDepth = 1
	c.Async = true
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 10})

	db := database.DBConnect
	db.Unscoped().Delete(&models.KidstaffCategory{})

	allCategpries := []models.KidstaffCategory{}
	existed_categories_ids := make(map[string]struct{})
	db.Find(&allCategpries)
	for _, cat := range allCategpries {
		existed_categories_ids[cat.Url] = struct{}{}
	}

	c.OnHTML("table.tablemain div.tditem-new", func(e *colly.HTMLElement) {
		temp_el := e.DOM.ChildrenFiltered("b").First()

		if !stringInSlice(temp_el.Text(), Config.AllowedCategoriesIds) {
			return
		}

		///////////////////////////

		parent_kc := models.KidstaffCategory{Depth: 1, Name: temp_el.Text(), Url: trim_goods(temp_el.Attr("href"))}
		db.Create(&parent_kc)
		var wg sync.WaitGroup
		wg.Add(1)
		go func(e *colly.HTMLElement, parent_kc models.KidstaffCategory, wg *sync.WaitGroup) {
			parent_list := e.DOM.Find("a:nth-child(2)")
			parent_list.Each(func(_ int, s *goquery.Selection) {
				parent_c := models.KidstaffCategory{Depth: 2, Name: s.Text(), Url: trim_goods(s.Attr("href"))}
				parent_c.ParentCategoryId = parent_kc.ID
				// db.Create(&parent_c)
				// db.Model(&parent_c).Association("ParentCategory").Append(parent_kc)
				db.Create(&parent_c)
				wg.Add(1)
				go func(l *goquery.Selection, parent_c models.KidstaffCategory, wg *sync.WaitGroup) {
					child_list := l.Parent().Unwrap().Find("ul > li > a")
					child_list.Each(func(_ int, s *goquery.Selection) {
						child_c := models.KidstaffCategory{Depth: 3, Name: s.Text(), Url: trim_goods(s.Attr("href"))}
						child_c.ParentCategoryId = parent_c.ID
						// db.Create(&child_c)
						// db.Model(&child_c).Association("ParentCategory").Append(parent_c)
						db.Create(&child_c)
					})
					wg.Done()
				}(s, parent_c, wg)
			})
			wg.Done()
		}(e, parent_kc, &wg)
		wg.Wait()
	})
	c.Visit("https://www.kidstaff.com.ua/")
	c.Wait()
}
