package image_downloader

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/corpix/uarand"
	"github.com/fatih/color"
)

const expectCommand = `#!/usr/bin/expect -f
set timeout %d
spawn rsync --delete-after -az %s/ %s@%s:%s
expect "%s@%s's password:"
send "%s\r"
expect eof
sleep 1
exit`

type ImageSyncerSettings struct {
	BaseDir    string
	RemoteDir  string
	RemoteUser string
	RemoteHost string
	RemotePwd  string
	Timeout    int
}

type fileDriver interface {
	saveImage(name string, data []byte)
	getImageList() []string
}

type fileDownloader interface {
	downloadImage(url string) ([]byte, error)
}

type RsyncSyncer struct {
	downloader fileDownloader
	driver     fileDriver
	existing   map[string]struct{}
	settings   ImageSyncerSettings
	wg         sync.WaitGroup
	lock       *sync.RWMutex
	queue      chan string
}

func (s *RsyncSyncer) SyncFile(fileUrl string) {
	s.wg.Add(1)
	color.Green("Adding %s to queue", fileUrl)
	s.queue <- fileUrl
}

func (s *RsyncSyncer) runWorker() {
	for fileUrl := range s.queue {
		s.lock.Lock()
		color.Green("Received %s from queue\n", fileUrl)
		filename := path.Base(fileUrl)
		if _, ok := s.existing[filename]; ok {
			return
		}
		if fileUrl == "" {
		}
		for i := 0; i < 2; i++ {
			res, err := s.downloader.downloadImage(fileUrl)
			if err != nil {
				time.Sleep(60 * time.Second)
				continue
			}
			s.driver.saveImage(filename, res)
			s.existing[filename] = struct{}{}
			break
		}
		s.wg.Done()
		s.lock.Unlock()
		time.Sleep(1 * time.Second)
	}
}

func (s *RsyncSyncer) Commit() {
	s.wg.Wait()
	color.Green("Preparing to sync")
	path, err := filepath.Abs(s.settings.BaseDir)
	if err != nil {
		log.Fatalln(err)
	}
	err = ioutil.WriteFile("temp.exp", []byte(fmt.Sprintf(expectCommand,
		s.settings.Timeout,
		path,
		s.settings.RemoteUser,
		s.settings.RemoteHost,
		s.settings.RemoteDir,
		s.settings.RemoteUser,
		s.settings.RemoteHost,
		s.settings.RemotePwd)),
		0766)
	color.Green("Created temp file")
	if err != nil {
		log.Fatalln(err)
	}
	md := exec.Command("expect", "temp.exp")
	res, _ := md.CombinedOutput()
	color.Green(string(res))
	err = os.Remove("temp.exp")
	if err != nil {
		log.Fatalln(err)
	}
	color.Green("Synced images!")
}

type simpleDownloader struct {
}

func (d *simpleDownloader) downloadImage(url string) ([]byte, error) {
	color.Green("Downloading image: %s, %t", url, !(strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://")))
	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	req.Header.Set("User-Agent", uarand.GetRandom())

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return body, nil
}

type fsDriver struct {
	basePath string
}

func (d *fsDriver) saveImage(name string, data []byte) {
	color.Green("Saving file to %s", d.basePath+"/"+name)
	err := ioutil.WriteFile(d.basePath+"/"+name, data, 0644)
	if err != nil {
		log.Fatalln(err)
	}
}

func (d *fsDriver) getImageList() []string {
	files, err := ioutil.ReadDir(d.basePath)
	if err != nil {
		log.Fatalln(err)
	}
	res := make([]string, 0)
	for _, v := range files {
		res = append(res, v.Name())
	}

	return res
}

func newSimpleDownloader() *simpleDownloader {
	return &simpleDownloader{}
}

func newFsDriver(basePath string) *fsDriver {
	if basePath == "" {
		basePath = "."
	}
	path, err := filepath.Abs(basePath)
	color.Green("Set base path to %s", path)
	if err != nil {
		log.Fatalln(err)
	}

	return &fsDriver{basePath: path}
}

func NewRsyncSyncer(s ImageSyncerSettings) *RsyncSyncer {
	fd := newFsDriver(s.BaseDir)
	cache := make(map[string]struct{})
	for _, v := range fd.getImageList() {
		cache[v] = struct{}{}
	}
	sync := &RsyncSyncer{
		downloader: newSimpleDownloader(),
		driver:     fd,
		existing:   cache,
		settings:   s,
		lock:       &sync.RWMutex{},
		queue:      make(chan string, 1024),
	}

	for i := 0; i < 5; i++ {
		go sync.runWorker()
	}

	return sync
}
