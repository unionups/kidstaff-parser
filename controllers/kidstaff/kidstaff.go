package kidstaff

import (
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	"github.com/parsers/kidstaff-parser/controllers/application"
	"github.com/parsers/kidstaff-parser/controllers/kidstaff/by_category"
	"github.com/parsers/kidstaff-parser/database"
	// "github.com/parsers/kidstaff-parser/models"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	// "time"
	"sync"
)

type KidstaffConfig struct {
	AllowedCategoriesIds []string `yaml:"allowed_categories_ids"`
}

var (
	ApplicationConfig *application.AppConfig
	Config            KidstaffConfig
	KidstaffCollector *colly.Collector
)

func Init(applicationConfig *application.AppConfig) {
	data, err := ioutil.ReadFile("config/kidstaff.yml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err = yaml.Unmarshal([]byte(data), &Config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	ApplicationConfig = applicationConfig
	KidstaffCollector = colly.NewCollector(
		colly.AllowedDomains(applicationConfig.Collector.Allowed.Domains...),
		colly.CacheDir("cache"),
	)
	// add storage to the collector
	KidstaffCollector.SetStorage(database.RedisStorage)
	extensions.RandomUserAgent(KidstaffCollector)

	// delete previous data from storage
	if err := database.RedisStorage.Clear(); err != nil {
		log.Fatal(err)
	}

	kidstaffGetRegionsAndCities()
	kidstaffGetCategories()

}

func Run() {
	database.RedisStorage.Clear()
	defer database.RedisStorage.Clear()

	var wg sync.WaitGroup
	wg.Add(1)
	go by_category.RemoveDataGenerator(&wg)
	by_category.KidsClothing(KidstaffCollector)
	wg.Wait()
}

func stringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}
