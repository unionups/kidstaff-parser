package application

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
)

type CollectorConfig struct {
	Allowed struct {
		Domains []string `yaml:"domains"`
	}
	DisableCeepAlives bool `yaml:"disable_keep_alive"`
	Async             bool `yaml:"async"`
}
type ShedulerConfig struct {
	IsEnable   bool   `yaml:"is_enable"`
	RunEvery   uint64 `yaml:"run_every"`
	BySpecific string `yaml:"by_specific"`
	AtTime     string `yaml:"at_time"`
}

type AppConfig struct {
	Collector CollectorConfig
	Sheduler  ShedulerConfig
	Xml       struct {
		ReturnedXmlFileName        string `yaml:"returned_xml_file_name"`
		ReturnedDeleteJsonFileName string `yaml:"returned_delete_json_file_name"`
	}
}

////// Extract config from YAML config file (config/application.yml) and init main collector
func Init(applicationConfig *AppConfig) {
	data, err := ioutil.ReadFile("config/application.yml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err = yaml.Unmarshal([]byte(data), &applicationConfig)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
}
