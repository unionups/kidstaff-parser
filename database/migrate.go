package database

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/lib/pq"
	"github.com/parsers/kidstaff-parser/models"
	"gopkg.in/gormigrate.v1"
	"time"
)

func migrate() {
	db := GetDB()
	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		// create besplatka_regions table
		{
			ID: "101608301401",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type BesplatkaRegion struct {
					gorm.Model
					RegionId        string                 `json:"region_id" binding:"required" gorm:"unique;not null;primary_key"`
					Name            string                 `json:"name" binding:"required" gorm:"unique;not null"`
					BesplatkaCities []models.BesplatkaCity `gorm:"foreignkey:RegionId;association_foreignkey:RegionId""`
				}
				return tx.AutoMigrate(&BesplatkaRegion{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("besplatka_regions").Error
			},
		},
		// create besplatka_cities table
		{
			ID: "201608301404",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type BesplatkaCity struct {
					gorm.Model
					CityId          string                 `json:"id" gorm:"unique;not null;primary_key"`
					RegionId        string                 `json:"region_id" binding:"required" gorm:"not null"`
					Name            string                 `json:"name" binding:"required" gorm:"not null"`
					BesplatkaRegion models.BesplatkaRegion `gorm:"foreignkey:RegionId"`
				}
				return tx.AutoMigrate(&BesplatkaCity{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("besplatka_cities").Error
			},
		},
		// create besplatka_categories table
		{
			ID: "301608311408",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type BesplatkaCategory struct {
					gorm.Model
					IdItem      string         `json:"id_item" gorm:"unique;not null;"`
					Depth       string         `json:"depth" gorm:"not null;"`
					ChildrenIds pq.StringArray `gorm:"type:varchar(100)[]"`
					Name        string         `json:"name"  gorm:"not null;"`
					Last        bool           `json:"last"`

					ParentCategoryId string
				}
				return tx.AutoMigrate(&BesplatkaCategory{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("besplatka_categories").Error
			},
		},
		// create kidstaff_regions table
		{
			ID: "401608301409",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type KidstaffRegion struct {
					gorm.Model
					RegionId string `gorm:"not null";primary_key`
					Name     string `gorm:"not null"`
				}
				return tx.AutoMigrate(&KidstaffRegion{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("kidstaff_regions").Error
			},
		},
		// create kidstaff_cities table
		{
			ID: "501608301410",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type KidstaffCity struct {
					gorm.Model
					CityId         string                `gorm:"unique;not null;primary_key"`
					CityURL        string                `gorm:"not null"`
					RegionId       string                `gorm:"not null"`
					Name           string                `gorm:"not null"`
					KidstaffRegion models.KidstaffRegion `gorm:"foreignkey:RegionId"association_foreignkey:RegionId`
				}
				return tx.AutoMigrate(&KidstaffCity{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("kidstaff_cities").Error
			},
		},
		// create kidstaff_categories table
		{
			ID: "601608301417",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type KidstaffCategory struct {
					gorm.Model
					Depth            int               `gorm:"not null;"`
					Name             string            `gorm:"not null;"`
					Url              string            `gorm:"not null;unique;"`
					Last             bool              `sql:"default:false"`
					ParentCategory   *KidstaffCategory `gorm:"foreignkey:ParentCategoryId;"`
					ParentCategoryId string
				}
				return tx.AutoMigrate(&KidstaffCategory{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("kidstaff_categories").Error
			},
		},
		// create besplatka_rules table
		{
			ID: "702758301417",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type BesplatkaRule struct {
					gorm.Model

					CityId             string `gorm:"not null;primary_key"`
					CategoryId         string `gorm:"not null;primary_key"`
					CategoryTreePath   string `gorm:"not null"`
					KidstaffCategoryId string `gorm:"not null"`

					RuleJSON     []byte         `sql:"type:json" gorm:"not null"`
					RequiredTags pq.StringArray `gorm:"type:varchar(64)[]"`
				}
				return tx.AutoMigrate(&BesplatkaRule{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("besplatka_rules").Error
			},
		},
		// create kidstaff_products table
		{
			ID: "802758301418",
			Migrate: func(tx *gorm.DB) error {
				// it's a good pratice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time
				type KidstaffProduct struct {
					ID        string `gorm:"primary_key"`
					CreatedAt time.Time
					UpdatedAt time.Time
					DeletedAt *time.Time `sql:"index"`
					URL       string     `gorm:"not null"`
					Category  string     `gorm:"not null"`
				}
				return tx.AutoMigrate(&KidstaffProduct{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("kidstaff_products").Error
			},
		},
	})

	if err := m.Migrate(); err != nil {
		log.Fatalf("Could not migrate: %v", err)
	}
	log.Printf("Migration did run successfully")

}
