package database

import (
	"fmt"
	"github.com/gocolly/redisstorage"
	"log"
	"os"
	"strconv"
)

func InitRedisStorage(dbConfig DBConfig) *redisstorage.Storage {

	log.Println("Redis Database init")
	var (
		redisStorage *redisstorage.Storage
		password     string
		host         string
		port         string
		database     string
		prefix       string
		ok           bool
	)

	if password, ok = os.LookupEnv("REDIS_PASS"); !ok {
		password = dbConfig.Password
	} else {
		password = "kidstaff-parser123"
	}
	if host, ok = os.LookupEnv("REDIS_HOST"); !ok {
		host = dbConfig.Host
	} else {
		host = "redis"
	}
	if port, ok = os.LookupEnv("REDIS_PORT"); !ok {
		port = dbConfig.Port
	} else {
		port = "6379"
	}
	if prefix, ok = os.LookupEnv("REDIS_PREFIX"); !ok {
		prefix = dbConfig.Prefix
	} else {
		prefix = "kidstaff-parser_development"
	}
	if database, ok = os.LookupEnv("REDIS_DATABASE"); !ok {
		database = dbConfig.Database
	} else {
		database = "0"
	}

	adress := fmt.Sprintf("%s:%s",
		host,
		port,
	)

	db, _ := strconv.Atoi(database)
	fmt.Printf("Redis Database: %s selected", database)
	redisStorage = &redisstorage.Storage{
		Address:  adress,
		Password: password,
		DB:       db,
		Prefix:   prefix,
	}

	log.Println("Redis Database connected")
	return redisStorage

}
