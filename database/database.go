package database

import (
	// "fmt"
	"github.com/gocolly/redisstorage"
	"github.com/jinzhu/gorm"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type DBConfig struct {
	Adapter  string `yaml:"adapter"`
	User     string `yaml:"username"`
	Password string `yaml:"password"`
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Database string `yaml:"database"`
	Prefix   string `yaml:"prefix"`
}

type DBEnvironments struct {
	Development DBConfig `yaml:"development"`
	Production  DBConfig `yaml:"production"`
}

type DBDeploys struct {
	Docker DBEnvironments `yaml:"docker"`
	Local  DBEnvironments `yaml:"local"`
}

var (
	DBConnect    *gorm.DB
	RedisStorage *redisstorage.Storage
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getDatabaseConfig(configFilePath string) DBConfig {
	var dbConfig DBConfig

	env := getEnv("ENV", "local.development")
	log.Printf("Kidstaff Parser RUN on %v environment", env)

	data, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	var dbDeploys DBDeploys
	err = yaml.Unmarshal([]byte(data), &dbDeploys)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	// fmt.Printf("%+v\n", dbDeploys)

	switch env {
	case "development":
		dbConfig = dbDeploys.Docker.Development
		log.Printf("dbConfig: %+v\n", dbConfig)
	case "production":
		dbConfig = dbDeploys.Docker.Production
	case "local.production":
		dbConfig = dbDeploys.Local.Production
	default:
		dbConfig = dbDeploys.Local.Development
		log.Printf("dbConfig: %+v\n", dbConfig)

	}
	return dbConfig
}

func InitDBs() {
	DBConnect = InitPostgresDB(getDatabaseConfig("config/database.yml"))
	RedisStorage = InitRedisStorage(getDatabaseConfig("config/redis.yml"))
}

func DBsClose() {
	DBConnect.Close()
	RedisStorage.Client.Close()
}

func GetDB() *gorm.DB {
	return DBConnect
}

func Migrate() {
	migrate()
}
