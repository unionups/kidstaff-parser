package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"log"
	"os"
)

func InitPostgresDB(dbConfig DBConfig) *gorm.DB {
	var dbConnect *gorm.DB
	if dbConfig.Adapter == "postgres" {

		log.Println("Postgres Database init")

		var (
			err      error
			user     string
			password string
			host     string
			port     string
			database string
			ok       bool
		)

		if user, ok = os.LookupEnv("PG_USER"); !ok {
			user = dbConfig.User
		} else {
			user = "postgres"
		}
		if password, ok = os.LookupEnv("PG_PASSWORD"); !ok {
			password = dbConfig.Password
		} else {
			password = ""
		}
		if host, ok = os.LookupEnv("PG_HOST"); !ok {
			host = dbConfig.Host
		} else {
			host = "postgres"
		}
		if port, ok = os.LookupEnv("PG_PORT"); !ok {
			port = dbConfig.Port
		} else {
			port = "5432"
		}
		if database, ok = os.LookupEnv("PG_DB"); !ok {
			database = dbConfig.Database
		} else {
			database = "kidstaff_parser_development"
		}

		// dbinfo := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		// 	user,
		// 	password,
		// 	host,
		// 	port,
		// 	database,
		// )

		dbinfo := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
			user,
			password,
			host,
			port,
			database,
		)
		// fmt.Println("PGdbinfo", dbinfo)

		//connect to postgres Database
		dbConnect, err = gorm.Open("postgres", dbinfo)

		//where myhost is port is the port postgres is running on
		//user is your postgres use name
		//password is your postgres password
		if err != nil {
			panic(err)
			panic("failed to connect database")
		}

		log.Println("Database connected")

	}
	return dbConnect

}
